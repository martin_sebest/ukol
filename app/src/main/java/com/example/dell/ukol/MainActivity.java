package com.example.dell.ukol;

import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.XMLFormatter;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    final String urlPath2 = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico=27756246";
    final String urlPathFirstPart = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico=";
    static String ICOnumber = new String();

    EditText text;
    TextView ico;
    Button button;

    DatabaseHelper db = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        button = (Button) findViewById(R.id.button);
        text = (EditText) findViewById(R.id.editText);
        ico = (TextView)findViewById(R.id.ICOView);
        //String vypis = new String("nazev");
        // final String urlPathSecondPart = new String();
       // String urlPathMerged = new String();

        db.getWritableDatabase();
        db.addIco(new ICO(1,"5","adresa"));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);

                /* Format URL */
                String urlPathSecondPart = text.getText().toString();
                String urlPathMerged = urlPathFirstPart + urlPathSecondPart;
                ICOnumber = urlPathSecondPart;

                /* Find in db otherwise on web */
                if (db.getICOCount() != 0){
                 ICO foundInDb = db.getICO(ICOnumber);
                    if(foundInDb.getId() != 0)
                    {
                        ico.setText(foundInDb.getAdress());
                    }else
                    {
                        new GetItems().execute(urlPathMerged);
                    }
                }else {


                    new GetItems().execute(urlPathMerged);
                }
                v.setEnabled(true);


            }
        });
    }

    private class GetItems extends AsyncTask<String,Void,Void> {
        String slovo = new String();
        @Override
        protected Void doInBackground(String... params) {
            XMLParser parser = new XMLParser();
            String xml = parser.getXMLFromUrl(params[0]);
            Document doc = parser.getDomElement(xml);

            //NodeList nl = doc.getElementsByTagName("dtt:Nazev_obce");
            NodeList nl = doc.getElementsByTagName("are:Odpoved");

            Element e = (Element) nl.item(0);
            //slovo = parser.getValue(e, "dtt:Nazev_obce");
            slovo = parser.getValue(e, "dtt:Error_text");
            db.addIco(new ICO(ICOnumber,slovo));


            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid){
            super.onPostExecute(aVoid);
            ico.setText(slovo);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
