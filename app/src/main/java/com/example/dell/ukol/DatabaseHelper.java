package com.example.dell.ukol;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ico_database_name";
    private static final String TABLE_ICO = "ico";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "ico_number";
    private static final String KEY_ADRESS = "adress";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ICO_TABLE = "CREATE TABLE " + TABLE_ICO + " (" + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_NAME + " TEXT, " + KEY_ADRESS + " TEXT" + ")";
        db.execSQL(CREATE_ICO_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ICO);

        onCreate(db);
    }

    /* Add new ICO */
    void addIco(ICO ico){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, ico.getIco());
        values.put(KEY_ADRESS, ico.getAdress());

        db.insert(TABLE_ICO, null, values);
        db.close();

    }

    /* Get ICO according to id */
    ICO getICO(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ICO, new String[]{KEY_ID, KEY_NAME, KEY_ADRESS}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
        }

        ICO ico = new ICO(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));

        return ico;
    }

    /* Get ICO according to ICO number */
    ICO getICO(String icoNumber){
        ICO ico = new ICO (0,"","");
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ICO, new String[]{KEY_ID, KEY_NAME, KEY_ADRESS}, KEY_NAME + "=?",
                new String[]{icoNumber}, null, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
        }

        if(cursor.getCount() >= 1 ) {
            //ICO ico = new ICO(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
            ico.setId(Integer.parseInt(cursor.getString(0)));
            ico.setIco(cursor.getString(1));
            ico.setAdress(cursor.getString(2));
        }
        else {
        }
        return ico;
    }

    /* Update ICO - one record*/
    public int updateICO(ICO ico) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, ico.getIco());
        values.put(KEY_ADRESS, ico.getAdress());

        return db.update(TABLE_ICO, values, KEY_ID + "=?",
                new String[]{String.valueOf(ico.getId())});
    }

    /* delete ICO */
    public void deleteICO(ICO ico){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ICO, KEY_ID + "=?",
                new String[]{String.valueOf(ico.getId())});
    }

    /* get count of records */
    public int getICOCount(){
        String countQuery = "SELECT * FROM " + TABLE_ICO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();

        return cursor.getCount();
    }

    /* get all ICOs */
    public List<ICO> getAllICOs(){
        List<ICO> ICOList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_ICO;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){
            do{
                ICO ico = new ICO();
                ico.setId(Integer.parseInt(cursor.getString(0)));
                ico.setIco(cursor.getString(1));
                ico.setAdress(cursor.getString(2));

                ICOList.add(ico);
            } while (cursor.moveToNext());
        }
        return ICOList;
    }

}


