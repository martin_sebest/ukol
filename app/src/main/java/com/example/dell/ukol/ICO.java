package com.example.dell.ukol;


public class ICO  {

    public static int id;
    public static String ico;
    public static String adress;

    public ICO(){

    }

    public ICO(String ico, String adress){
        this.ico = ico;
        this.adress = adress;
    }

    public ICO(int id, String ico, String adress){
        this.id = id;
        this.ico = ico;
        this.adress = adress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}


